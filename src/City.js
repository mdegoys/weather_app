import React from 'react';

const City = (props) => {
	return (
		<div className="city-card">
			<div>Ville : {props.name}</div>
			<div>Temperature : {props.tempDisplay == 'C' ? props.temp_c : props.temp_f} º{props.tempDisplay}</div>
		</div>

	)
}

export default City;
