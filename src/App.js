import React, { Component } from 'react';
import City from './City';
import './App.css';

class App extends Component {
	state = {
		newCityName : '',
		cities: [
			{ name: "Paris", temp_c: 30, temp_f: 0 },
			{ name: "London", temp_c: 23, temp_f: 0 },
		],
		tempDisplay: 'C'
	}

	setNewCityName = (e) => {
		this.setState({	newCityName: e.currentTarget.value });
	}

	addNewCity = () => {
		const newCityName = this.state.newCityName;
		this.setState({ newCityName: '', cities: [...this.state.cities, {name: newCityName, temp_c:0, temp_f: 0}] })
		this.fetchCityData(newCityName)
	}

	fetchCityData = (cityName) => {
		fetch('http://api.apixu.com/v1/current.json?key=9e938ae41b174e518de130750192901&q='+cityName)
		.then(response => response.json())
		.then(result =>	this.setState({ cities: this.state.cities.map(x => x.name === cityName ? {name: cityName, temp_c : result.current.temp_c, temp_f: result.current.temp_f } : x) } ))

	}

	switchTempDisplay = () => {
		const newTempDisplay = this.state.tempDisplay === 'C' ? 'F' : 'C';
		this.setState({ tempDisplay: newTempDisplay });
	}

	componentDidMount() {
		this.state.cities.forEach(x => this.fetchCityData(x.name))
	}	


  render() {
    return (
      <div className="App">
				<h1>Météo</h1>
				<div id="city-cards">
					{this.state.cities.map(x => <City key={x.name} name={x.name} temp_c={x.temp_c} temp_f={x.temp_f} tempDisplay={this.state.tempDisplay} />)}
				</div>

				<h2>Add a city : </h2>
				<input type="text" name="newCity" onChange={this.setNewCityName} value={this.state.newCityName} />
				<button onClick={this.addNewCity}>Add !</button>

				<h2>Switch temperature display :</h2>
				<button onClick={this.switchTempDisplay}>Switch !</button>
     </div>
    );
  }
}

export default App;
